package com.zibawu.authentication.service;

import com.zibawu.authentication.model.User;

public interface UserService {
    User findByEmail(String email);

    void saveUser(User user);
}
INSERT INTO user (id, first_name, middle_name, last_name, email, password, gender, id_number, phone_number, birth_date, joining_date,
 employee_bank, branch, current_position, grade, membership_number, zibawu_branch, status, qualifications, tshirt_size) VALUES (1,
 'John', '', 'Smith', 'johnsmith@gmail.com', '$2a$10$RyY4bXtV3LKkDCutlUTYDOKd2AiJYZGp4Y7MPVdLzWzT1RX.JRZyG', 'female', '66-000000 X11',
 '+263 77 100 0000', '12/12/1990', '12/12/2012', 'Barclays', 'Jason Moyo', 'Bank Teller' '5', '00000000', 'Harare', 'active', 'Bsc Acounting', 'M' );

INSERT INTO role (id, name) VALUES (1, 'ROLE_ADMIN');
INSERT INTO role (id, name) VALUES (2, 'ROLE_MANAGER');
INSERT INTO role (id, name) VALUES (3, 'ROLE_USER');

INSERT INTO users_roles (user_id, role_id) VALUES (1, 1);
INSERT INTO users_roles (user_id, role_id) VALUES (1, 2);